package rests;

import da.ProductDAService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class RestServiceController {

    @Inject
    private ProductDAService productDAService;

    @GET
    @Path("catalog")
    public Response getCatalog() {
        return productDAService.getFromBd();
    }

    @GET
    @Path("catalog/withFilter")
    public Response getByFilter(@DefaultValue("empty") @QueryParam("name") String name) {
        return productDAService.getByFilter(name);
    }

    @GET
    @Path("product/{id}")
    public Response getById(@PathParam("id") Long idToSearch) {
        return productDAService.getById(idToSearch);
    }

    @GET
    @Path("catalog/actualProducts")
    public Response getActualProducts() {
        return productDAService.getActualProducts();
    }

    @Path("create")
    @GET
    public Response createProduct(@DefaultValue("DefaultName") @QueryParam("name") String name,
                                  @DefaultValue("1") @QueryParam("price") Double price,
                                  @DefaultValue("DefaultCategory") @QueryParam("category") String category) {
        return productDAService.create(name, price, category);
    }
}
