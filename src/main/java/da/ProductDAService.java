package da;

import beans.Product;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

@Named
public class ProductDAService {
    EntityManagerFactory emf;
    EntityManager em;

    public ProductDAService() {
    }

    @PostConstruct
    void prepare() {
        emf = Persistence.createEntityManagerFactory("eclipseJPA");
        em = emf.createEntityManager();
    }

    @PreDestroy
    void destroy() {
        em.close();
        emf.close();
    }

    public Response getFromBd() {

        Query query = em.createQuery("SELECT t FROM Product t");
        List<Product> list = query.getResultList();
        em.close();

        return Response.ok(list).build();
    }

    public Response getById(Long id) {
        Product product = em.find(Product.class, id);
        em.close();

        return Response.ok(product).build();
    }

    public Response getByFilter(String name) {
        Query query = em.createQuery("SELECT t FROM Product t WHERE t.name LIKE :name").setParameter("name", name);
        List<Product> list = query.getResultList();
        em.close();

        return Response.ok(list).build();
    }

    public Response getActualProducts() {
        Query query = em.createQuery("SELECT distinct t.category,t.name,t.price FROM Product t");
        List<Product> list = query.getResultList();
        em.close();

        return Response.ok(list).build();
    }

    public Response create(String name, Double price, String category) {
        Product product1 = new Product(name, price, category);

        em.getTransaction().begin();
        em.persist(product1);
        em.getTransaction().commit();
        return Response.ok(Collections.EMPTY_LIST).build();
    }
}
